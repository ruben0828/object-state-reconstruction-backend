class CreateMyObjects < ActiveRecord::Migration[5.1]
  def change
    create_table :my_objects do |t|
      t.integer :object_id
      t.string :object_type
      t.datetime :timestamp
      t.text :object_changes
    end
  end
end
