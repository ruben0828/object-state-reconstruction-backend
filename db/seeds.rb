# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
MyObject.create!([
		{
			object_id: 1,
			object_type: 'Order',
			timestamp: Time.at(1484730554),
			object_changes: "{\"customer_name\":\"Jack\",\"customer_address\":\"Trade St.\",\"status\":\"unpaid\"}"
		},
		{
			object_id: 2,
			object_type: 'Order',
			timestamp: Time.at(1484730623),
			object_changes: "{\"customer_name\":\"Sam\",\"customer_address\":\"Gecko St.\",\"status\":\"unpaid\"}"
		},
		{
			object_id: 1,
			object_type: 'Product',
			timestamp: Time.at(1484731172),
			object_changes: "{\"name\":\"Laptop\",\"price\":2100,\"stock_levels\":29}"
		},
		{
			object_id: 1,
			object_type: 'Order',
			timestamp: Time.at(1484731481),
			object_changes: "{\"status\":\"paid\",\"ship_date\":\"2017-01-18\",\"shipping_provider\":\"DHL\"}"
		},
		{
			object_id: 2,
			object_type: 'Product',
			timestamp: Time.at(1484731671),
			object_changes: "{\"name\":\"Microphones\",\"price\":160,\"stock_levels\":1500}"
		},
		{
			object_id: 1,
			object_type: 'Invoice',
			timestamp: Time.at(1484731920),
			object_changes: "{\"order_id\":7,\"product_ids\":[1,5,3],\"status\":\"unpaid\",\"total\":2500}"
		},
		{
			object_id: 1,
			object_type: 'Invoice',
			timestamp: Time.at(1484732821),
			object_changes: "{\"status\":\"paid\"}"
		}
	])