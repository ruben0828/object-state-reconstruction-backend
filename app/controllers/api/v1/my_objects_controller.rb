module Api
  module V1
    class MyObjectsController < ApplicationController

      # GET /my_objects
      def index
        my_objects = MyObject.where(object_id: params[:id])
                      .where(object_type: params[:type])
                      .where("timestamp < ?", Time.at(params[:timestamp].to_i))
                      .order(timestamp: :asc)

        @states = my_objects.inject({}) do |res, obj|
          res = res.merge(JSON.parse(obj.object_changes))
        end

        json_response(@states)
      end

      # POST /my_objects/import
      def import
        MyObject.import(params[:file])
        json_response({}, :created)
      end

      # DELETE /my_objects/delete_all
      def destroy_all
        MyObject.destroy_all
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_my_object
          @my_object = MyObject.find(params[:id])
        end

        # Only allow a trusted parameter "white list" through.
        def my_object_params
          params.require(:my_object).permit(:object_id, :object_type, :timestamp, :object_changes)
        end
    end
  end
end
