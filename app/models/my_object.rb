require 'csv'
class MyObject < ApplicationRecord
	validates :object_id, presence: true
	validates :object_type, presence: true
	validates :timestamp, presence: true

	def self.import(file)
		header = []
		File.foreach(file.path) do |csv|
			row = CSV.parse(csv.gsub('\"','""')).first
			if header.empty?
				header = row.map(&:to_sym)
				next
			end

			row = Hash[header.zip(row)].to_hash
			obj = MyObject.new
			obj.attributes = row
			obj.timestamp = Time.at(row[:timestamp].to_i)
			obj.save
			puts "OBJ: #{obj.timestamp}"
		end
	end
end