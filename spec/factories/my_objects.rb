FactoryBot.define do
	factory :my_object do

		factory :obj1 do
			add_attribute(:object_id) { 1 }
			object_type "Order"
			timestamp { Time.at(1484730554) }
			object_changes { "{\"customer_name\":\"Jack\",\"customer_address\":\"Trade St.\",\"status\":\"unpaid\"}" }
		end

		factory :obj2 do
			add_attribute(:object_id) { 2 }
			object_type "Order"
			timestamp { Time.at(1484730623) }
			object_changes { "{\"customer_name\":\"Sam\",\"customer_address\":\"Gecko St.\",\"status\":\"unpaid\"}" }
		end

		factory :obj3 do
			add_attribute(:object_id) { 1 }
			object_type "Product"
			timestamp { Time.at(1484731172) }
			object_changes { "{\"name\":\"Laptop\",\"price\":2100,\"stock_levels\":29}" }
		end

		factory :obj4 do
			add_attribute(:object_id) { 1 }
			object_type "Order"
			timestamp { Time.at(1484731481) }
			object_changes { "{\"status\":\"paid\",\"ship_date\":\"2017-01-18\",\"shipping_provider\":\"DHL\"}" }
		end

		factory :obj5 do
			add_attribute(:object_id) { 2 }
			object_type "Product"
			timestamp { Time.at(1484731671) }
			object_changes { "{\"name\":\"Microphones\",\"price\":160,\"stock_levels\":1500}" }
		end

		factory :obj6 do
			add_attribute(:object_id) { 1 }
			object_type "Invoice"
			timestamp { Time.at(1484731920) }
			object_changes { "{\"order_id\":7,\"product_ids\":[1,5,3],\"status\":\"unpaid\",\"total\":2500}" }
		end

		factory :obj7 do
			add_attribute(:object_id) { 1 }
			object_type "Invoice"
			timestamp { Time.at(1484732821) }
			object_changes { "{\"status\":\"paid\"}" }
		end
	end
end
