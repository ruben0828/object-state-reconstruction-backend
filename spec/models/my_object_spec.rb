require 'rails_helper'

RSpec.describe MyObject, type: :model do
	it { should validate_presence_of(:object_id) }
	it { should validate_presence_of(:object_type) }
	it { should validate_presence_of(:timestamp) }
end