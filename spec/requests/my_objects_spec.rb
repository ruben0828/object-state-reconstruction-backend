require 'rails_helper'

RSpec.describe 'MyObjects API', type: :request do

	# initialize test data
	let!(:obj1) { create(:obj1) }
	let!(:obj2) { create(:obj2) }
	let!(:obj3) { create(:obj3) }
	let!(:obj4) { create(:obj4) }
	let!(:obj5) { create(:obj5) }
	let!(:obj6) { create(:obj6) }
	let!(:obj7) { create(:obj7) }

	# Test suite for GET /api/v1/my_objects
	describe 'GET /api/v1/my_objects/1/type/Order/timestamp/1484733173' do

		before { get '/api/v1/my_objects/1/type/Order/timestamp/1484733173' }

		it 'returns my_objects' do
			expect(json).not_to be_empty
			expect(json['customer_name']).to eq("Jack")
			expect(json['customer_address']).to eq("Trade St.")
			expect(json['status']).to eq("paid")
			expect(json['ship_date']).to eq("2017-01-18")
			expect(json['shipping_provider']).to eq("DHL")
		end

		it 'returns status code 200' do
			expect(response).to have_http_status(200)
		end
	end

	describe 'GET /api/v1/my_objects/1/type/Order/timestamp/1484722542' do

		before { get '/api/v1/my_objects/1/type/Order/timestamp/1484722542' }

		it 'returns empty' do
			expect(json).to be_empty
		end

		it 'returns status code 200' do
			expect(response).to have_http_status(200)
		end
	end

	describe 'GET /api/v1/my_objects/1/type/Order/timestamp/1484731400' do

		before { get '/api/v1/my_objects/1/type/Order/timestamp/1484731400' }

		it 'returns my_objects' do
			expect(json).not_to be_empty
			expect(json['customer_name']).to eq("Jack")
			expect(json['customer_address']).to eq("Trade St.")
			expect(json['status']).to eq("unpaid")
		end

		it 'returns status code 200' do
			expect(response).to have_http_status(200)
		end
	end

	describe 'GET /api/v1/my_objects/1/type/Invoice/timestamp/1516269000' do

		before { get '/api/v1/my_objects/1/type/Invoice/timestamp/1516269000' }

		it 'returns my_objects' do
			expect(json).not_to be_empty
			expect(json['status']).to eq("paid")
		end

		it 'returns status code 200' do
			expect(response).to have_http_status(200)
		end
	end

	# Test suite for POST /api/v1/my_objects/import
	describe 'POST /api/v1/my_objects/import' do

		it 'returns 201' do
			expect(MyObject).to receive(:import).with("objects.csv")
			post "/api/v1/my_objects/import", params: { file: "objects.csv" }
		end
	end
end