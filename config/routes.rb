Rails.application.routes.draw do
	namespace :api do
		namespace :v1 do
			get '/my_objects/:id/type/:type/timestamp/:timestamp', to: 'my_objects#index'
			post '/my_objects/import', to: 'my_objects#import'
			delete '/my_objects/delete_all', to: 'my_objects#delete_all'
		end
	end
end
